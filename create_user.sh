#!/bin/bash


USERNAME="$2"
PASSWORD="$3"


addUser()
{
	#add user acct with name passed in as first parameter($2)
	sudo useradd -m $USERNAME

	#Check if the length of the password is zero, if so generate random password
	if [ -z $PASSWORD ];
	then
		PASSWORD=$(openssl rand -base64 32)
	fi
	#set passwd for user acct using second parameter...

	echo "$USERNAME:$PASSWORD" | sudo chpasswd && echo "Account $USERNAME has been created!"

	#Write USERNAME and PASSWORD in a file
	touch credentials.txt
	echo -e "Welcome to our company! Here are your credentials \nUsername:$USERNAME \nPassword:$PASSWORD" >> credentials.txt

	#send email with user credentials to new user and removing the credentials file for security reasons
	mail -A credentials.txt -s "Here are your login credentials" maneclangpatrick03@gmail.com < /dev/null && \
		echo "Mail has been delivered" && \
		echo "Deleting credentials file" && \
		rm -rf credentials.txt

	#Copy company rules to new users home directory
	sudo cp company_rules.txt /home/$USERNAME/
}


deleteUser()
{
	sudo userdel $USERNAME && sudo rm -rf /home/$USERNAME && \
	echo "$USERNAME has been deleted from the system"
}

if [ "$1" == "add" ]
then
	addUser
elif [ "$1" == "delete" ]
then
	deleteUser
else
	echo "You must enter add or delete as first argument"
fi
